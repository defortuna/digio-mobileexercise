//
//  NetworkingError.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 26/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import Foundation

enum NetworkingError: Error {
    case noInternetConnection
    case dataNotFound
    case serverUnavailable
    case corruptedData
    case somethingWentWrong
}

extension NetworkingError: CustomStringConvertible {
    var description: String {
        switch self {
        case .noInternetConnection:
            return "You seam to be offline. Check your internet connection and try later"
        case .dataNotFound:
            return "Data not found!"
        case .serverUnavailable:
            return "The server is unreachable. Please, try again later"
        case .corruptedData:
            return "Data currupted! "
        case .somethingWentWrong:
            return "Something went wrong! Please, try again later"
        }
    }
}
