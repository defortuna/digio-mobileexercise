//
//  NetworkingService.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 22/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import Foundation

struct NetworkingService {
    
    fileprivate init(){}
    static let shared = NetworkingService()
    fileprivate let session = URLSession(configuration: .default)
    
    func fetchData<T: Codable>(forEndPoint endPoint: EndPoints,
                               andFormat format: T.Type,
                               callback: @escaping (Result<T, NetworkingError>) -> () ) {
        if let url = URL(string: endPoint.rawValue) {
            session.dataTask(with: url) { (data, response, error) in
                if let nsError = error as? NSError, nsError.code == -1009 {
                    //-1009: NSError code for no internet connection
                    callback(.failure(.noInternetConnection))
                }
                        
                if let httpResponse = response as? HTTPURLResponse, let data = data {
                    //check statusCode:
                    switch httpResponse.statusCode {
                    case 200...299:
                        //success:
                        let decoder = JSONDecoder()
                        if let newData = try? decoder.decode(format, from: data) {
                            callback(.success(newData))
                        } else {
                            callback(.failure(.corruptedData))
                        }
                    case 300...499:
                        //redirection messages(300...399) and client error(400...499)
                        //data has been moved to another server or wrong request
                        callback(.failure(.dataNotFound))
                    case 500...599:
                        //server issues
                        callback(.failure(.serverUnavailable))
                    default:
                        //something else went wrong
                        callback(.failure(.somethingWentWrong))
                    }
                }
            }.resume()
        } else {
            callback(.failure(.somethingWentWrong))
        }
    }
}






