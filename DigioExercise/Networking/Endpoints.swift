//
//  Endpoints.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 22/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import Foundation

enum EndPoints: String {
    case home = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products"
}
