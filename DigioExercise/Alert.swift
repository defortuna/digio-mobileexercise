//
//  Alert.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 26/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import Foundation
import UIKit

struct Alert {
    
    fileprivate init() {}
    
    static func call(withMessage message: String?, andTitle title: String?, onViewController vc: UIViewController?) {
        guard let vc = vc else { return }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        DispatchQueue.main.async {
            vc.present(alert, animated: true, completion: nil)
        }
    }
}
