//
//  SpotlightItemCollectionViewCell.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 22/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import UIKit

class SpotlightItemCollectionViewCell: UICollectionViewCell {
    
    fileprivate var spotlight: Spotlight?
    fileprivate var spotlightImageView = UIImageView()
    fileprivate var skimmer = SkeletonService()
    
    func configureCell(forSpotlight spotlight: Spotlight) {
        self.applyCornerRadiusAndShadow()
        formatImageView()
        self.spotlight = spotlight
        
        spotlightImageView.load(urlString: spotlight.bannerURL) { (_) in
            self.skimmer.stopAnimation(forView: self.spotlightImageView)
        }
        configureSkimmer()
    }
    
    fileprivate func formatImageView() {
        self.addSubview(spotlightImageView)
        spotlightImageView.anchorEdges(top: self.topAnchor,
                                       left: self.leftAnchor,
                                       right: self.rightAnchor,
                                       bottom: self.bottomAnchor,
                                       padding: .zero)
        spotlightImageView.layer.cornerRadius = 10
        spotlightImageView.contentMode = .scaleAspectFill
        spotlightImageView.clipsToBounds = true
    }
    
    fileprivate func configureSkimmer() {
        skimmer.addSupportingViewsForAnimation(self)
        skimmer.addViewForAnimation(spotlightImageView)
        skimmer.animateViews()
    }
}
