//
//  SpotlightTableViewCell.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 22/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import UIKit

protocol SpotlightCellDelegate: class {
    func handleSpotlightCellSelection(spotlightItem: Spotlight, spotlightCellHeight: CGFloat)
}

class SpotlightTableViewCell: UITableViewCell {

    fileprivate var spotlight = [Spotlight]()
    fileprivate let spotlightCellHeight: CGFloat = 150
    weak var delegate: SpotlightCellDelegate?
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        layout.scrollDirection = .horizontal
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = Colors.background.color
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
       }()
    
    fileprivate let cellId = "cellId"
    
    func configureCell(forSpotlight spotlight: [Spotlight]) {
        self.spotlight = spotlight
        self.selectionStyle = .none
        configureCollectionView()
        collectionView.reloadData()
    }
    
    fileprivate func configureCollectionView() {
        self.addSubview(collectionView)
        collectionView.anchorEdges(top: self.safeAreaLayoutGuide.topAnchor,
                                   left: self.safeAreaLayoutGuide.leftAnchor,
                                   right: self.safeAreaLayoutGuide.rightAnchor,
                                   bottom: self.safeAreaLayoutGuide.bottomAnchor,
                                   padding: .init(top: 0, left: 8, bottom: -8, right: 0))
        collectionView.anchorSizes(sizeWidth: nil, sizeHeight: 180)
        collectionView.register(SpotlightItemCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
    }
}

extension SpotlightTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        spotlight.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let spotlightItem = spotlight[indexPath.row]
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? SpotlightItemCollectionViewCell else { return UICollectionViewCell() }
        cell.configureCell(forSpotlight: spotlightItem)
        return cell
    }
}

extension SpotlightTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let spotlightItem = spotlight[indexPath.row]
        delegate?.handleSpotlightCellSelection(spotlightItem: spotlightItem,
                                               spotlightCellHeight: spotlightCellHeight)
    }
}

extension SpotlightTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.frame.width - 35 , height: spotlightCellHeight)
    }
}
