//
//  ProductTableViewCell.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 22/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import UIKit

protocol ProductCellDelegate: class {
    func handleProductCellSelection(productItem: Product, productCellHeight: CGFloat)
}

class ProductTableViewCell: UITableViewCell {

    fileprivate var product = [Product]()
    fileprivate var productLabel = UILabel()
    fileprivate let productCellHeight: CGFloat = 130
    weak var delegate: ProductCellDelegate?
    fileprivate var skimmer = SkeletonService()
    
    fileprivate lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        layout.scrollDirection = .horizontal
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = Colors.background.color
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
       }()
    
    fileprivate let cellId = "cellId"
    
    func configureCell(forProduct product: [Product]) {
        self.selectionStyle = .none
        self.product = product
        configureProductLabel()
        configureCollectionView()
        configureSkimmer()
        collectionView.reloadData()
    }
    
    fileprivate func configureProductLabel() {
        self.addSubview(productLabel)
        productLabel.anchorEdges(top: self.topAnchor,
                              left: self.leftAnchor,
                              right: self.rightAnchor,
                              bottom: nil,
                              padding: .init(top: 28, left: 8, bottom: 0, right: -8))
        productLabel.anchorSizes(sizeWidth: nil, sizeHeight: 30)
        productLabel.formatForSectionTitle(withText: "Produtos")
    }
    
    fileprivate func configureCollectionView() {
        self.addSubview(collectionView)
        collectionView.anchorEdges(top: productLabel.bottomAnchor,
                                   left: productLabel.leftAnchor,
                                   right: self.rightAnchor,
                                   bottom: self.safeAreaLayoutGuide.bottomAnchor,
                                   padding: .init(top: 0, left: 0, bottom: -8, right: 0))
        collectionView.anchorSizes(sizeWidth: nil, sizeHeight: 150)
        collectionView.register(ProductItemCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    fileprivate func configureSkimmer() {
        skimmer.addSupportingViewsForAnimation(productLabel)
        skimmer.animateViews()
    }
}

extension ProductTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return product.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let productItem = product[indexPath.row]
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? ProductItemCollectionViewCell else { return UICollectionViewCell() }
        cell.configureCell(forProduct: productItem, skimmer: skimmer)
        return cell
    }
}

extension ProductTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productItem = product[indexPath.row]
        delegate?.handleProductCellSelection(productItem: productItem, productCellHeight: productCellHeight)
    }
}

extension ProductTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: productCellHeight , height: productCellHeight)
    }
}
