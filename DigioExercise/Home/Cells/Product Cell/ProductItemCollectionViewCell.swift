//
//  ProductItemCollectionViewCell.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 23/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import UIKit

class ProductItemCollectionViewCell: UICollectionViewCell {
    
    fileprivate var product: Product?
    fileprivate var productImageView = UIImageView()
    
    func configureCell(forProduct product: Product, skimmer: SkeletonService) {
        formatImageView()
        setSkimmer(forSkimmer: skimmer)
        self.product = product
        self.applyCornerRadiusAndShadow()
        productImageView.load(urlString: product.imageURL) { [weak self] (_) in
            skimmer.stopAnimation(forView: self?.productImageView)
        }
        
    }
    
    fileprivate func formatImageView() {
        self.addSubview(productImageView)
        productImageView.anchorSizes(sizeWidth: 80, sizeHeight: 80)
        productImageView.anchorCenters(centerX: self.centerXAnchor, centerY: self.centerYAnchor)
        productImageView.contentMode = .scaleAspectFit
        productImageView.clipsToBounds = true
    }
    
    fileprivate func setSkimmer(forSkimmer skimmer: SkeletonService) {
        skimmer.addSupportingViewsForAnimation(self)
        skimmer.addViewForAnimation(productImageView)
        skimmer.animateViews()
    }
}



