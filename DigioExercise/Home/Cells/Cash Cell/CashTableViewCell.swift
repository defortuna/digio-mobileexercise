//
//  CashTableViewCell.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 22/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import UIKit

class CashTableViewCell: UITableViewCell {
    
    fileprivate var cashLabel = UILabel()
    fileprivate var cashImage = UIImageView()
    fileprivate var cash: Cash?
    fileprivate var skimmer = SkeletonService()
    
    
    let imageHeight: CGFloat = 100
    
    func setUI(forCash cash: Cash) {
        self.selectionStyle = .none
        self.cash = cash
        formatCashLabel()
        formatCashImage()
        setSkimmer()
        
        cashImage.load(urlString: cash.bannerURL, callback: { [weak self] (_) in
            self?.skimmer.stopAnimation(forView: self?.cashImage)
        })
    }
    
    fileprivate func formatCashLabel() {
        self.addSubview(cashLabel)
        cashLabel.anchorEdges(top: self.topAnchor,
                              left: self.leftAnchor,
                              right: self.rightAnchor,
                              bottom: nil,
                              padding: .init(top: 8, left: 8, bottom: 0, right: -8))
        cashLabel.anchorSizes(sizeWidth: nil, sizeHeight: 30)
        cashLabel.formatForSectionTitle(withText: "digio Cash")
    }
    
    fileprivate func formatCashImage() {
        self.addSubview(cashImage)
        cashImage.anchorEdges(top: cashLabel.bottomAnchor,
                              left: self.leftAnchor,
                              right: self.rightAnchor,
                              bottom: self.bottomAnchor,
                              padding: .init(top: 8, left: 8, bottom: -8, right: -8))
        cashImage.anchorSizes(sizeWidth: nil, sizeHeight: imageHeight)
        cashImage.layer.cornerRadius = 15
        cashImage.contentMode = .scaleAspectFill
        cashImage.clipsToBounds = true
    }
    
    fileprivate func setSkimmer() {
        skimmer.addSupportingViewsForAnimation(cashLabel)
        skimmer.addViewForAnimation(cashImage)
        skimmer.animateViews()
    }

}
