//
//  HomeViewController.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 22/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import UIKit

struct HomeCells {
    static let allCells = ["SpotlightTableViewCell", "CashTableViewCell", "ProductTableViewCell"]
    static let spotlight = "SpotlightTableViewCell"
    static let cash = "CashTableViewCell"
    static let product = "ProductTableViewCell"
}

class HomeViewController: UIViewController {
    
    // MARK: - Constants
    fileprivate let networkingService = NetworkingService.shared
    fileprivate let cells = HomeCells.allCells
    fileprivate let tableView = UITableView()
    fileprivate var skimmer = SkeletonService()
    
    // MARK: - Variables
    fileprivate var homedata: HomeData?
    fileprivate var iconImageView = UIImageView()
    fileprivate var userNameLabel = UILabel()
    var userName = String()
    
    // MARK: - View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.background.color
        self.title = "Home"
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        configureWelcomeMessage()
        configureTableView()
        setupSkimmer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        userNameLabel.text = "Olá, \(userName)"
        showSkimmer()
        fetchHomeData()
    }

    // MARK: - Configuring Gretting

    fileprivate func configureWelcomeMessage() {
        setIconImageView()
        setUserName()
    }
    
    fileprivate func setIconImageView() {
        view.addSubview(iconImageView)
        iconImageView.anchorEdges(top: view.safeAreaLayoutGuide.topAnchor,
                                  left: view.safeAreaLayoutGuide.leftAnchor,
                                  right: nil,
                                  bottom: nil,
                                  padding: .init(top: 10, left: 10, bottom: 0, right: 0))
        iconImageView.anchorSizes(sizeWidth: 40, sizeHeight: 40)
        iconImageView.contentMode = .scaleAspectFit
        iconImageView.image = Icons.loginIcon.image
    }
    
    fileprivate func setUserName() {
        view.addSubview(userNameLabel)
        userNameLabel.anchorEdges(top: iconImageView.topAnchor,
                                  left: iconImageView.rightAnchor,
                                  right: nil,
                                  bottom: iconImageView.bottomAnchor,
                                  padding: .init(top: 0, left: 8, bottom: 0, right: 0))
    }


    // MARK: - Configuring Table View
    fileprivate func configureTableView() {
        self.view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        tableView.anchorEdges(top: iconImageView.bottomAnchor,
                              left: iconImageView.leftAnchor,
                              right: view.safeAreaLayoutGuide.rightAnchor,
                              bottom: view.safeAreaLayoutGuide.bottomAnchor,
                              padding: .zero)
        
        tableView.register(SpotlightTableViewCell.self, forCellReuseIdentifier: HomeCells.spotlight)
        tableView.register(CashTableViewCell.self, forCellReuseIdentifier: HomeCells.cash)
        tableView.register(ProductTableViewCell.self, forCellReuseIdentifier: HomeCells.product)
    }
    
    // MARK: - Fetching data
    fileprivate func fetchHomeData() {
        networkingService.fetchData(forEndPoint: .home, andFormat: HomeData.self) {  [weak self] results in
                switch results {
                case .success(let homeData):
                    self?.formatResult(forData: homeData)

                case .failure(let error):
                    Alert.call(withMessage: error.description, andTitle: nil, onViewController: self)
                }
        }
    }
    
    fileprivate func formatResult(forData data: HomeData) {
        DispatchQueue.main.async {
            self.hideSkimmer()
            self.homedata = data
            self.tableView.reloadData()
        }
    }
    
    // MARK: - SETTING UP SKIMMER
    
    fileprivate func setupSkimmer() {
        skimmer.addViewForAnimation(iconImageView)
        skimmer.addViewForAnimation(userNameLabel)
    }
    
    fileprivate func showSkimmer() {
        skimmer.animateViews()
    }
    
    fileprivate func hideSkimmer() {
        skimmer.stopAnimation(forView: iconImageView)
        skimmer.stopAnimation(forView: userNameLabel)
    }
    
    fileprivate func pushDetailViewController<T: Codable>(forItem item: T, height: CGFloat) {
        switch item {
        case is Spotlight:
            let spotlightViewController = SpotlightDetailViewController()
            spotlightViewController.configure(forSpotlightItem: item as! Spotlight, height: height)
            self.navigationController?.pushViewController(spotlightViewController, animated: true)
        case is Cash:
            let cashViewController = CashDetailViewController()
            cashViewController.configure(forCashItem: item as! Cash, height: height)
            self.navigationController?.pushViewController(cashViewController, animated: true)
        case is Product:
            let productViewController = ProductDetailViewController()
            productViewController.configure(forProductItem: item as! Product, height: height)
            self.navigationController?.pushViewController(productViewController, animated: true)
        default:
            break
        }
    }
}

    // MARK: - Extensions

extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let data = homedata else { return UITableViewCell() }
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCells.spotlight) as! SpotlightTableViewCell
            cell.configureCell(forSpotlight: data.spotlight)
            cell.delegate = self
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCells.cash) as! CashTableViewCell
            cell.setUI(forCash: data.cash)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeCells.product) as! ProductTableViewCell
            cell.configureCell(forProduct: data.products)
            cell.delegate = self
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cash = homedata?.cash else { return }
        if let cell = tableView.cellForRow(at: indexPath) as? CashTableViewCell {
            pushDetailViewController(forItem: cash, height: cell.imageHeight)
        }
    }
}

extension HomeViewController: SpotlightCellDelegate {
    func handleSpotlightCellSelection(spotlightItem: Spotlight, spotlightCellHeight: CGFloat) {
        pushDetailViewController(forItem: spotlightItem, height: spotlightCellHeight)
    }
}

extension HomeViewController: ProductCellDelegate {
    func handleProductCellSelection(productItem: Product, productCellHeight: CGFloat) {
        pushDetailViewController(forItem: productItem, height: productCellHeight)
    }
}
