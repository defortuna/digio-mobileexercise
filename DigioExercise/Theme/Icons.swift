//
//  Icons.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 23/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import Foundation
import UIKit

enum Icons {
    case loginIcon
    case imageNotFound
    
    var image: UIImage {
        switch self {
        case .loginIcon:
            return #imageLiteral(resourceName: "icon")
        case .imageNotFound:
            return UIImage(systemName: "photo") ?? UIImage()
        }
    }
}
