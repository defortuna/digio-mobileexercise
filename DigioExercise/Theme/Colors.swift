//
//  Colors.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 23/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import Foundation
import UIKit

enum Colors {
    case mainTitle
    case secondaryTitle
    case background
    case text
    
    var color: UIColor {
        switch self {
        case .mainTitle:
            return UIColor(named: "mainTitle") ?? UIColor()
        case .secondaryTitle:
            return UIColor(named: "secondaryTitle") ?? UIColor()
        case .background:
            return UIColor(named: "background") ?? UIColor()
        case .text:
            return UIColor(named: "text") ?? UIColor()
        }
    }
}
