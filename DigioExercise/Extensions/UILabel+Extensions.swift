//
//  UILabel+Extensions.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 23/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func formatForSectionTitle(withText text: String) {
        
        //adding font to attributed string
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(.font, value: UIFont(name: "Helvetica-Bold", size: 26) as Any,
                                      range: NSRange(location: 0, length: text.count))
        
        let words = text.components(separatedBy: " ")
        
        //Adding colors
        
        
        if !words[0].isEmpty {
            attributedString.addAttribute(.foregroundColor, value: Colors.mainTitle.color,
                                              range: NSRange(location: 0, length: words[0].count))
            
        }

        if words.count > 1, !words[1].isEmpty {
            attributedString.addAttribute(.foregroundColor, value: Colors.secondaryTitle.color,
                                              range: NSRange(location: words[0].count + 1 , length: words[1].count))
            
        }
        self.attributedText = attributedString
    }
}
