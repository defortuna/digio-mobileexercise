//
//  HomeData.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 22/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import Foundation

struct HomeData: Codable {
    let spotlight: [Spotlight]
    let products: [Product]
    let cash: Cash
}
