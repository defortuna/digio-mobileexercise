//
//  Product.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 22/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import Foundation

struct Product: Codable {
    let name: String
    let imageURL: String
    let description: String
}
