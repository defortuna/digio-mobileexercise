//
//  Spotlight.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 22/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import Foundation

struct Spotlight: Codable {
    let name: String
    let bannerURL: String
    let description: String
}
