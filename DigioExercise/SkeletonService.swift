//
//  SkeletonService.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 26/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import Foundation
import UIKit
import SkeletonView

class SkeletonService {

    private var skeletonViews = [UIView]()
    private var supportingViews = [UIView]()
    
    func addViewForAnimation(_ newView: UIView) {
        skeletonViews.append(newView)
        newView.isSkeletonable = true
    }
    
    func addSupportingViewsForAnimation(_ newView: UIView) {
        newView.isSkeletonable = true
        supportingViews.append(newView)
    }
    
    func animateViews() {
        for view in skeletonViews {
            view.showAnimatedGradientSkeleton()
        }
        for sView in supportingViews {
            sView.showAnimatedGradientSkeleton()
        }
    }
    
    func stopAnimation(forView view: UIView?) {
        let loadView = skeletonViews.filter { $0 == view}.first
        if let loadView = loadView {
            loadView.hideSkeleton()
            stopSupportingViewsAnimations()
        }
    }
    
    private func stopSupportingViewsAnimations() {
        for view in supportingViews {
            view.hideSkeleton()
        }
    }
}
