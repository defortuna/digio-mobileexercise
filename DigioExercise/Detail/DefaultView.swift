//
//  DefaultView.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 23/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import UIKit

class DefaultView: UIView {

    fileprivate var profilePhoto = UIImageView()
    fileprivate var itemDescription = UITextView()
    
    func formatUI(forPhotoUrl stringUrl: String, andDescription description: String, height: CGFloat) {
        configureProfilePhoto(url: stringUrl, height: height)
        configureItemDescription(description: description)
        
    }
    
    fileprivate func configureProfilePhoto(url: String, height: CGFloat) {
        self.addSubview(profilePhoto)
        profilePhoto.anchorEdges(top: self.topAnchor,
                                 left: self.leftAnchor,
                                 right: self.rightAnchor,
                                 bottom: nil,
                                 padding: .zero)
        profilePhoto.anchorSizes(sizeWidth: nil, sizeHeight: height)
        profilePhoto.backgroundColor = .white
        profilePhoto.contentMode = .scaleToFill
        profilePhoto.load(urlString: url) { (_) in }
    }
    
    fileprivate func configureItemDescription(description: String) {
        self.addSubview(itemDescription)
        itemDescription.anchorEdges(top: profilePhoto.bottomAnchor,
                                    left: self.leftAnchor,
                                    right: self.rightAnchor,
                                    bottom: self.bottomAnchor,
                                    padding: .init(top: 10, left: 10, bottom: 0, right: -10))
        itemDescription.anchorSizes(sizeWidth: nil, sizeHeight: 400)
        itemDescription.text = description
        itemDescription.font = .preferredFont(forTextStyle: .body)
        itemDescription.textColor = Colors.text.color
    }
}
