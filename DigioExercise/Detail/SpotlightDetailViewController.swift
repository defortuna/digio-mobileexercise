//
//  SpotlightDetailViewController.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 23/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import UIKit

class SpotlightDetailViewController: UIViewController {
    
    fileprivate var spotlight: Spotlight?
    fileprivate let defaultView = DefaultView()
    fileprivate var imageHeight: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.background.color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
        guard let spotlight = spotlight else { return }
        self.title = spotlight.name
        formatUI()
    }
    
    func configure(forSpotlightItem item: Spotlight, height: CGFloat) {
        self.spotlight = item
        imageHeight = height
    }
    
    fileprivate func formatUI() {
        view.addSubview(defaultView)
        guard let spotlight = spotlight else { return }
    
        defaultView.anchorEdges(top: view.safeAreaLayoutGuide.topAnchor,
                                left: view.safeAreaLayoutGuide.leftAnchor,
                                right: view.safeAreaLayoutGuide.rightAnchor,
                                bottom: view.safeAreaLayoutGuide.bottomAnchor,
                                padding: .zero)
        
        defaultView.formatUI(forPhotoUrl: spotlight.bannerURL,
                             andDescription: spotlight.description,
                             height: imageHeight)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        defaultView.removeFromSuperview()
    }
}
