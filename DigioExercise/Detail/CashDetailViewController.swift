//
//  CashDetailViewController.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 23/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import UIKit

class CashDetailViewController: UIViewController {
    
    fileprivate var cash: Cash?
    fileprivate let defaultView = DefaultView()
    fileprivate var imageHeight: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.background.color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
        guard let cash = cash else { return }
        self.title = cash.title
        formatUI()
    }
    
    func configure(forCashItem item: Cash, height: CGFloat) {
        self.cash = item
        imageHeight = height
    }
    
    fileprivate func formatUI() {
        view.addSubview(defaultView)
        guard let cash = cash else { return }
    
        defaultView.anchorEdges(top: view.safeAreaLayoutGuide.topAnchor,
                                left: view.safeAreaLayoutGuide.leftAnchor,
                                right: view.safeAreaLayoutGuide.rightAnchor,
                                bottom: view.safeAreaLayoutGuide.bottomAnchor,
                                padding: .zero)
        
        defaultView.formatUI(forPhotoUrl: cash.bannerURL,
                             andDescription: cash.description,
                             height: imageHeight)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        defaultView.removeFromSuperview()
    }
}
