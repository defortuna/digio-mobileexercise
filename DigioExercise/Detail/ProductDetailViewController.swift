//
//  ProductDetailViewController.swift
//  DigioExercise
//
//  Created by Denis Fortuna on 23/10/20.
//  Copyright © 2020 Denis Fortuna. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIViewController {
    
    fileprivate var product: Product?
    fileprivate let defaultView = DefaultView()
    fileprivate var imageHeight: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.background.color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
        guard let product = product else { return }
        self.title = product.name
        formatUI()
    }
    
    func configure(forProductItem item: Product, height: CGFloat) {
        self.product = item
        imageHeight = height
    }
    
    fileprivate func formatUI() {
        view.addSubview(defaultView)
        guard let product = product else { return }
    
        defaultView.anchorEdges(top: view.safeAreaLayoutGuide.topAnchor,
                                left: view.safeAreaLayoutGuide.leftAnchor,
                                right: view.safeAreaLayoutGuide.rightAnchor,
                                bottom: view.safeAreaLayoutGuide.bottomAnchor,
                                padding: .zero)
        
        defaultView.formatUI(forPhotoUrl: product.imageURL,
                             andDescription: product.description,
                             height: imageHeight)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        defaultView.removeFromSuperview()
    }
}
