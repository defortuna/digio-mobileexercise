# Digio-MobileExercise

Digio-MobileExercise is an iOS application developed using Swift as an attempt to the Digio mobile code challenge


## Libraries
*  [SkeletonView](https://github.com/Juanpe/SkeletonView)

## Requiriments
*  iOS 13
*  Swift 5.1
*  Xcode 11.0 or later
*  [Cocoapods](https://cocoapods.org)

## Author
*  Denis Fortuna 
